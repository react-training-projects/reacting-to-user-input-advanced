import axios from 'axios';

const KEY = 'AIzaSyACK64os3KJWhAvgHMto5YC3mfjwUzM1d0';

export default axios.create({
  baseURL: 'https://www.googleapis.com/youtube/v3',
  params: {
    part: 'snippet',
    maxResults: 5,
    key: KEY
  }
});
