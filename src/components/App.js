import React from 'react';
import youtube from '../apis/youtube';
import SearchBar from './SearchBar';
import VideoList from './VideoList';
import VideoDetail from './VideoDetail';

class App extends React.Component {
  state = {
    videos: [],
    selected: null
  };

  doSearch = async (text) => {
    var response = await youtube.get('/search', {
      params: {
        q: text
      }
    });

    this.setState({
      videos: response.data.items,
      selected: response.data.items[0]
    });
  };

  selectVideo = (video) => {
    this.setState({ selected: video });
  }

  componentDidMount() {
    this.doSearch('sports car racing');
  }

  render() {
    return (
      <div className="ui container">
        <SearchBar search={this.doSearch} />
        <div className="ui grid">
          <div className="ui row">
            <div className="eleven wide column">
              <VideoDetail video={this.state.selected} />
            </div>
            <div className="five wide column">
              <VideoList 
                videos={this.state.videos}
                selectVideo={this.selectVideo} />
            </div>
          </div>
        </div>
      </div>
    );
  }
}

export default App;
