import React from 'react';

const VideoDetail = ({ video }) => {
  var content = <div>Loading . . .</div>;

  if (video) {
    var videoSrc = `https://www.youtube.com/embed/${video.id.videoId}`;

    content = (
      <div className="video-detail">
        <div className="ui embed">
          <iframe 
            src={videoSrc}
            title="video player" />
        </div>
        <div className="ui segment">
          <h4 className="ui header">
            {video.snippet.title}
          </h4>
          <p>{video.snippet.description}</p>
        </div>
      </div>
    );
  }

  return content;
};

export default VideoDetail;
