import React from 'react';

class SearchBar extends React.Component {
  state = {
    text: ''
  };

  handleTextChange = (event) => {
    this.setState({ text: event.target.value })
  };

  handleFormSubmit = (event) => {
    event.preventDefault();

    this.props.search(this.state.text);
  };

  render() {
    return (
      <div className="search-bar ui segment">
        <form 
          className="ui form"
          onSubmit={(e) => this.handleFormSubmit(e)}
        >
          <div className="field">
            <label htmlFor="search-text">Video Search</label>
            <input 
              type="text"
              id="search-text"
              value={this.state.text}
              onChange={(e) => this.handleTextChange(e)}
            />
          </div>
        </form>
      </div>
    );
  }
}

export default SearchBar;
